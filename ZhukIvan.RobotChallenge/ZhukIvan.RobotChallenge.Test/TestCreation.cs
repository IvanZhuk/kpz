﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System;
using System.Collections.Generic;

namespace ZhukIvan.RobotChallenge.Test
{
    [TestClass]
    public class TestCreation
    {
        [TestMethod]
        public void TestClose()
        {
            var algorithm = new ZhukIvanAlgorithm();
            var map = new Map();
            var station1 = new EnergyStation() { Energy = 1000, Position = new Position(0, 0), RecoveryRate = 2 };
            var station2 = new EnergyStation() { Energy = 1000, Position = new Position(20, 20), RecoveryRate = 2 };
            map.Stations.Add(station1);
            map.Stations.Add(station2);

            var robot1 = new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1) };

            var robots = new List<Robot.Common.Robot>() { robot1 };

            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsInstanceOfType(command, typeof(CreateNewRobotCommand));
        }

        [TestMethod]
        public void TestFar()
        {
            var algorithm = new ZhukIvanAlgorithm();
            var map = new Map();
            var station1 = new EnergyStation() { Energy = 1000, Position = new Position(0, 0), RecoveryRate = 2 };
            var station2 = new EnergyStation() { Energy = 1000, Position = new Position(70, 70), RecoveryRate = 2 };
            map.Stations.Add(station1);
            map.Stations.Add(station2);

            var robot1 = new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1) };

            var robots = new List<Robot.Common.Robot>() { robot1 };

            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsInstanceOfType(command, typeof(CollectEnergyCommand));
        }
    }
}
