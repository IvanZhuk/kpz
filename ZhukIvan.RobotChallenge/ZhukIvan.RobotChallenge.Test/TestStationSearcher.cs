﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System;
using System.Collections.Generic;

namespace ZhukIvan.RobotChallenge.Test
{
    [TestClass]
    public class TestStationSearcher
    {
        [TestMethod]
        public void TestFreeSearch()
        {
            var algorithm = new ZhukIvanAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position() { X = 2, Y = 2}, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position() { X = 5, Y = 2 }, RecoveryRate = 2 });

            var robot1 = new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1), OwnerName = algorithm.Author };
            var robot2 = new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 3), OwnerName = algorithm.Author };

            var robots = new List<Robot.Common.Robot>() { robot1, robot2 };

            var command = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(new Position() { X = 4, Y = 1 }, ((MoveCommand)command).NewPosition);
        }

        [TestMethod]
        public void TestEnergySearch()
        {
            var algorithm = new ZhukIvanAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 0, Position = new Position() { X = 2, Y = 2 }, RecoveryRate = 0 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position() { X = 5, Y = 2 }, RecoveryRate = 0 });

            var robot1 = new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1), OwnerName = algorithm.Author };

            var robots = new List<Robot.Common.Robot>() { robot1 };

            var command = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(new Position() { X = 4, Y = 1 }, ((MoveCommand)command).NewPosition);
        }
    }
}
