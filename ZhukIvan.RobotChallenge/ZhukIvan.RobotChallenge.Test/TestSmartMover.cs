﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System;
using System.Collections.Generic;

namespace ZhukIvan.RobotChallenge.Test
{
    [TestClass]
    public class TestSmartMover
    {
        [TestMethod]
        public void TestGoalFinder()
        {
            var algorithm = new ZhukIvanAlgorithm();
            var map = new Map();
            var stationPosition = new Position(5, 5);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });

            var robot1 = new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1) };

            var robots = new List<Robot.Common.Robot>() { robot1 };

            var command = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(new Position() { X = 4, Y = 4 }, ((MoveCommand)command).NewPosition);
        }

        [TestMethod]
        public void TestOptimalMover()
        {
            var algorithm = new ZhukIvanAlgorithm();
            var map = new Map();
            var stationPosition = new Position(20, 20);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });

            var robot1 = new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1) };

            var robots = new List<Robot.Common.Robot>() { robot1 };

            var command = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(new Position() { X = 5, Y = 5 }, ((MoveCommand)command).NewPosition);
        }
    }
}
