﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System;
using System.Collections.Generic;

namespace ZhukIvan.RobotChallenge.Test
{
    [TestClass]
    public class TestClusters
    {
        [TestMethod]
        public void TestSearch()
        {
            var algorithm = new ZhukIvanAlgorithm();
            var map = new Map();

            EnergyStation es1 = new EnergyStation() { Energy = 1000, Position = new Position(3, 3), RecoveryRate = 2 };
            EnergyStation es2 = new EnergyStation() { Energy = 1000, Position = new Position(5, 3), RecoveryRate = 2 };
            map.Stations.Add(es1);
            map.Stations.Add(es2);

            var robot1 = new Robot.Common.Robot() { Energy = 100, Position = new Position(6, 3), OwnerName = algorithm.Author };
            var robots = new List<Robot.Common.Robot>() { robot1 };

            var command = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(4, ((MoveCommand)command).NewPosition.X);
        }
    }
}
