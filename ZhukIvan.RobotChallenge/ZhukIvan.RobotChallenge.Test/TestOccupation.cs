﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System;
using System.Collections.Generic;

namespace ZhukIvan.RobotChallenge.Test
{
    [TestClass]
    public class TestOccupation
    {
        [TestMethod]
        public void TestWithEmpty()
        {
            var algorithm = new ZhukIvanAlgorithm();

            var position = new Position(1, 1);
            var position2 = new Position(1, 2);
            var robot1 = new Robot.Common.Robot() { Energy = 200, Position = position, OwnerName = algorithm.Author };
            var robot2 = new Robot.Common.Robot() { Energy = 200, Position = new Position(10, 2), OwnerName = algorithm.Author };
            var robots = new List<Robot.Common.Robot>() { robot1, robot2 };

            var isFree = IsFree.FromAnyone(position, robot2, robots);
            Assert.AreEqual(false, isFree);

            isFree = IsFree.FromAnyone(position2, robot2, robots);
            Assert.AreEqual(true, isFree);
        }

        [TestMethod]
        public void TestWithAuthor()
        {
            var algorithm = new ZhukIvanAlgorithm();

            var position = new Position(1, 1);
            var position2 = new Position(1, 2);
            var robot1 = new Robot.Common.Robot() { Energy = 200, Position = position, OwnerName = algorithm.Author };
            var robot2 = new Robot.Common.Robot() { Energy = 200, Position = new Position(10, 2), OwnerName = algorithm.Author };
            var robots = new List<Robot.Common.Robot>() { robot1, robot2 };

            var isFree = IsFree.FromAuthor(position, robot2, robots);
            Assert.AreEqual(false, isFree);

            isFree = IsFree.FromAuthor(position2, robot2, robots);
            Assert.AreEqual(true, isFree);
        }

        [TestMethod]
        public void TestWithNoAuthor()
        {
            var algorithm = new ZhukIvanAlgorithm();

            var position = new Position(1, 1);
            var position2 = new Position(1, 2);
            var robot1 = new Robot.Common.Robot() { Energy = 200, Position = position, OwnerName = algorithm.Author };
            var robot2 = new Robot.Common.Robot() { Energy = 200, Position = new Position(10, 2), OwnerName = algorithm.Author };
            var robots = new List<Robot.Common.Robot>() { robot1, robot2 };

            var isFree = IsFree.FromAnyoneExceptAuthor(position, robot2, robots);
            Assert.AreEqual(true, isFree);

            isFree = IsFree.FromAnyoneExceptAuthor(position2, robot2, robots);
            Assert.AreEqual(true, isFree);
        }
    }
}
