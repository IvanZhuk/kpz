﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Collections.Specialized.BitVector32;

namespace ZhukIvan.RobotChallenge
{
    public class IsFree
    {
        public delegate bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots);

        public static EnergyStation FindClosestFreeStationWithEnergy(
            Position pos, Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots, IsFree.IsCellFree checker)
        {
            EnergyStation closest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFreeFrom(station, movingRobot, robots, checker) && station.Energy > 0)
                {
                    int d = DistanceHelper.FindDistance(station.Position, pos);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        closest = station;
                    }
                }
            }
            return closest ?? null;
        }

        public static EnergyStation FindClosestFreeStation(
            Position pos, Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots, IsFree.IsCellFree checker)
        {
            EnergyStation closest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFreeFrom(station, movingRobot, robots, checker))
                {
                    int d = DistanceHelper.FindDistance(station.Position, pos);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        closest = station;
                    }
                }
            }
            return closest ?? null;
        }

        public static bool IsStationFreeFrom(
            EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, IsFree.IsCellFree checker)
        {
            for (int i = -DistanceHelper.Reach; i <= DistanceHelper.Reach; i++)
            {
                for (int j = -DistanceHelper.Reach; j <= DistanceHelper.Reach; j++)
                {
                    Position t = new Position() { X = station.Position.X + j, Y = station.Position.Y + i };
                    if (!checker(t, movingRobot, robots))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool FromAnyone(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot && robot.Position == cell)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool FromAuthor(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot && robot.Position == cell && robot.OwnerName == movingRobot.OwnerName)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool FromAuthorAndSelf(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot.Position == cell && robot.OwnerName == movingRobot.OwnerName)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
