﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Collections.Specialized.BitVector32;

namespace ZhukIvan.RobotChallenge
{
    public class ClusterFinder
    {
        private const int checkingRange = 2;
        private const int valueRange = 1;
        private const int mapSize = 100;

        public static Position Search(EnergyStation station, Map map, Position currentPos)
        {
            int[,] valueMap = new int[mapSize, mapSize];
            RecursiveSearch(station, valueMap, map);
            return PositionWithMaxValue(valueMap, currentPos);
        }

        private static void RecursiveSearch(EnergyStation currentStation, int[,] valueMap, Map map)
        {
            for (int i = currentStation.Position.Y - valueRange; i <= currentStation.Position.Y + valueRange; i++)
            {
                for (int j = currentStation.Position.X - valueRange; j <= currentStation.Position.X + valueRange; j++)
                {
                    if (i < 0 || i >= mapSize || j < 0 || j >= mapSize)
                    {
                        continue;
                    }

                    valueMap[j, i] += 1;
                }
            }

            for (int i = currentStation.Position.Y - checkingRange; i <= currentStation.Position.Y + checkingRange; i++)
            {
                for (int j = currentStation.Position.X - checkingRange; j <= currentStation.Position.X + checkingRange; j++)
                {
                    if (i < 0 || i >= mapSize || j < 0 || j >= mapSize)
                    {
                        continue;
                    }

                    Position t = new Position() { X = j, Y = i };
                    foreach(var station in map.Stations)
                    {
                        if (station != currentStation && station.Position == t && valueMap[j, i] == 0 && station.Energy > 0)
                        {
                            RecursiveSearch(station, valueMap, map);
                        }
                    }
                }
            }

            return;
        }

        private static Position PositionWithMaxValue(int[,] valueMap, Position currentPos)
        {
            int maxValue = int.MinValue;
            Position result = new Position();

            for (int i = 0; i < mapSize; i++)
            {
                for (int j = 0; j < mapSize; j++)
                {
                    if (valueMap[j, i] > maxValue) 
                    { 
                        maxValue = valueMap[j, i];
                        result.X = j; 
                        result.Y = i;
                    }
                }
            }

            if (valueMap[currentPos.X, currentPos.Y] == maxValue)
            {
                return currentPos;
            }

            return result;
        }
    }
}
