﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ZhukIvan.RobotChallenge
{
    public class SmartMover
    {
        public static MoveCommand Move(Robot.Common.Robot robot, Position goal)
        {
            int steps = SmartMover.CheckSteps(robot, goal, 0);

            Position transitGoal = new Position()
            {
                X = robot.Position.X + (goal.X - robot.Position.X) / steps,
                Y = robot.Position.Y + (goal.Y - robot.Position.Y) / steps
            };

            return new MoveCommand() { NewPosition = transitGoal };
        }

        public static int CheckSteps(Robot.Common.Robot robot, Position goal, int saveEnergy)
        {
            int i = 1;
            int distance = DistanceHelper.FindDistance(robot.Position, goal);
            int transitDistance = distance;

            while (transitDistance * i > robot.Energy - saveEnergy)
            {
                i++;
                transitDistance = (int)Math.Pow(Math.Sqrt(distance) / i, 2);
            }

            return i;
        }

        public static Position FindGoal(EnergyStation station, Robot.Common.Robot robot, IList<Robot.Common.Robot> robots, Map map)
        {
            Position goalCell = new Position();

            if (IsFree.Station(station, robot, robots, IsFree.FromAnyone))
            {
                if (robot.Position.X - station.Position.X == 0)
                {
                    goalCell.X = 0;
                }
                else
                {
                    goalCell.X = (robot.Position.X - station.Position.X) / Math.Abs(robot.Position.X - station.Position.X);
                }
                goalCell.X += station.Position.X;

                if (robot.Position.Y - station.Position.Y == 0)
                {
                    goalCell.Y = 0;
                }
                else
                {
                    goalCell.Y = (robot.Position.Y - station.Position.Y) / Math.Abs(robot.Position.Y - station.Position.Y);
                }
                goalCell.Y += station.Position.Y;

                return goalCell;
            }
            else
            {
                return FindTheBastard(station, robot, robots);
            }
        }

        public static Position FindTheBastard(EnergyStation station, Robot.Common.Robot robot, IList<Robot.Common.Robot> robots)
        {
            for (int i = -DistanceHelper.Reach; i <= DistanceHelper.Reach; i++)
            {
                for (int j = -DistanceHelper.Reach; j <= DistanceHelper.Reach; j++)
                {
                    Position t = new Position() { X = station.Position.X + j, Y = station.Position.Y + i };

                    if (!IsFree.FromAnyoneExceptAuthor(t, robot, robots))
                    {
                        return t;
                    }
                }
            }
            return null;
        }
    }
}
