﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZhukIvan.RobotChallenge
{
    internal class FindClosest
    {
        public static EnergyStation FreeStationWithEnergy(
            Position pos, Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots, IsFree.IsCellFree checker)
        {
            EnergyStation closest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsFree.Station(station, movingRobot, robots, checker) && station.Energy > 0)
                {
                    int d = DistanceHelper.FindDistance(station.Position, pos);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        closest = station;
                    }
                }
            }
            return closest ?? null;
        }

        public static EnergyStation FreeStation(
            Position pos, Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots, IsFree.IsCellFree checker)
        {
            EnergyStation closest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsFree.Station(station, movingRobot, robots, checker))
                {
                    int d = DistanceHelper.FindDistance(station.Position, pos);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        closest = station;
                    }
                }
            }
            return closest ?? null;
        }
    }
}
