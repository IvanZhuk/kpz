﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;
using static System.Collections.Specialized.BitVector32;

namespace ZhukIvan.RobotChallenge
{
    public class ZhukIvanAlgorithm : IRobotAlgorithm
    {
        public string Author { get { return "Zhuk Ivan"; } }

        private static int round = 0;

        private static int robotCount = 10;

        public ZhukIvanAlgorithm()
        {
            Logger.OnLogRound += (object sender, LogRoundEventArgs e) => round++;
        }
                
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];

            EnergyStation closestStationWithEnergy = FindClosest.FreeStationWithEnergy(
                movingRobot.Position, movingRobot, map, robots, IsFree.FromAuthor);

            if (closestStationWithEnergy != null)
            {
                int distance = DistanceHelper.FindDistance(movingRobot.Position, closestStationWithEnergy.Position);

                if (distance <= 2)
                {
                    Position better = ClusterFinder.Search(closestStationWithEnergy, map, movingRobot.Position);
                    if (better != movingRobot.Position && IsFree.FromAuthor(better, movingRobot, robots))
                    {
                        return new MoveCommand() { NewPosition = better };
                    }

                    if (movingRobot.Energy >= 200 && robots.Count < map.Stations.Count && robotCount < 100)
                    {
                        EnergyStation closestFree = FindClosest.FreeStation(
                            movingRobot.Position, movingRobot, map, robots, IsFree.FromAuthorAndSelf);

                        if (SmartMover.CheckSteps(movingRobot, closestFree.Position, 10) <= Math.Min(25, 40 - round))
                        {
                            robotCount++;
                            return new CreateNewRobotCommand();
                        }
                    }

                    return new CollectEnergyCommand();
                }
                else
                {
                    Position goal = SmartMover.FindGoal(closestStationWithEnergy, movingRobot, robots, map);
                    return SmartMover.Move(movingRobot, goal);                 
                }
            }
            else
            {
                EnergyStation closestFree = FindClosest.FreeStation(
                    movingRobot.Position, movingRobot, map, robots, IsFree.FromAuthor);

                Position goal = SmartMover.FindGoal(closestFree, movingRobot, robots, map);
                return SmartMover.Move(movingRobot, goal);
            }
        }
    }
}
